using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Clickable : MonoBehaviour
{
    [field: SerializeField] public UnityEvent OnClick { get; private set; }

    protected virtual void OnMouseDown()
    {
        OnClick?.Invoke();
        Destroy(gameObject);
    }
}
