using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour
{
    [SerializeField] private ThresholdTimer thresholdTimer = null;
    [SerializeField] private Image timerImg = null;

    public void Update()
    {
        timerImg.fillAmount = thresholdTimer.CurrentTimeNormalized;
        timerImg.color = thresholdTimer.InTime ? Color.green : thresholdTimer.Early ? Color.blue : Color.red;

    }
}
