using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThresholdTimer : MonoBehaviour
{
    [SerializeField] private float maxTime = 3f;
    [SerializeField] [Range(0f, 1f)] private float minReadyThresholdFactor = 0.8f;

    public static event System.Action<ThresholdTimer> OnClick = null;

    private float timeSinceSpawn;
    public float CurrentTimeNormalized => timeSinceSpawn / maxTime;
    private float MinReadyThreshold => maxTime * minReadyThresholdFactor;
    public bool Early => timeSinceSpawn < MinReadyThreshold;
    public bool Late => timeSinceSpawn == maxTime;
    public bool InTime => !Early && !Late;

    public bool Stopped { get; set; }

    private bool lateTriggered;

    private void OnMouseDown()
    {
        if (Late) return;
        Stopped = true;
        OnClick?.Invoke(this);
    }

    private void Update()
    {
        if (!Stopped)
            timeSinceSpawn = Mathf.Min(timeSinceSpawn + Time.deltaTime, maxTime);
        if (!Late || lateTriggered) return;
        lateTriggered = true;
        Stopped = true;
        OnClick?.Invoke(this);
    }
}
