using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerGrid : MonoBehaviour
{
    [field: SerializeField] public Row[] Rows { get; private set; }
    [SerializeField] private ThresholdTimer[] objsToSpawn = new ThresholdTimer[0];
    [SerializeField] private float initialDwellBetweenSpawns = 2f;
    [SerializeField] private float minDwellBetweenSpawns = 0.5f;
    [SerializeField] private float dwellDecreaseRatio = 0.02f;

    public bool Spawn { get; set; } = true;

    private float dwellBetweenSpawns;

    private IEnumerator Start()
    {
        dwellBetweenSpawns = initialDwellBetweenSpawns;
        while (Spawn)
        {
            yield return new WaitForSeconds(dwellBetweenSpawns);
            dwellBetweenSpawns = Mathf.Max(dwellBetweenSpawns - dwellDecreaseRatio, minDwellBetweenSpawns);
            SpawnAtRandomPos();
        }
    }

    private void SpawnAtRandomPos()
    {
        var unusedSpawnPoints = new List<Transform>();
        foreach (var row in Rows)
            unusedSpawnPoints.AddRange(row.UnusedSpawnPoints);
        if (unusedSpawnPoints.Count == 0) return;
        var spawnPoint = unusedSpawnPoints[Random.Range(0, unusedSpawnPoints.Count)];
        Instantiate(objsToSpawn[Random.Range(0, objsToSpawn.Length)], spawnPoint);
    }

}
