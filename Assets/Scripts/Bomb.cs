using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] private Animator anim = null;
    [SerializeField] private ThresholdTimer timer = null;

    private void Awake()
    {
        ThresholdTimer.OnClick += HandleClick;
    }

    private void OnDestroy()
    {
        ThresholdTimer.OnClick -= HandleClick;
    }

    private void HandleClick(ThresholdTimer timer)
    {
        if (this.timer != timer) return;
        Destroy(gameObject, timer.InTime ? 0 : anim.GetCurrentAnimatorStateInfo(0).length);
        anim.enabled = !timer.InTime;
    }
}
