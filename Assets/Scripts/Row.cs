using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Row : MonoBehaviour
{
    [field: SerializeField] public List<Transform> SpawnPoints { get; private set; }

    public IEnumerable<Transform> UnusedSpawnPoints => SpawnPoints.FindAll(s => s.childCount == 0);
}
