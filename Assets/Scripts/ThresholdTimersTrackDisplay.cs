using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThresholdTimersTrackDisplay : MonoBehaviour
{
    [SerializeField] private Text earlyText = null;
    [SerializeField] private Text inTimeText = null;
    [SerializeField] private Text lateText = null;

    private void Awake()
    {
        ThresholdTimersTracker.OnThresholdTimerTracked += HandleThresholdTimerTracked;
    }

    private void OnDestroy()
    {
        ThresholdTimersTracker.OnThresholdTimerTracked -= HandleThresholdTimerTracked;
    }

    private void HandleThresholdTimerTracked(int earlyAmount, int inTimeAmount, int lateAmount)
    {
        earlyText.text = $"Early: {earlyAmount}";
        inTimeText.text = $"In time: {inTimeAmount}";
        lateText.text = $"Late: {lateAmount}";
    }

}
