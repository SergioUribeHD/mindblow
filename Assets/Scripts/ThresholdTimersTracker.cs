using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThresholdTimersTracker : MonoBehaviour
{
    public static event System.Action<int, int, int> OnThresholdTimerTracked = null;

    private int earlyAmount;
    private int inTimeAmount;
    private int lateAmount;

    private void Awake()
    {
        ThresholdTimer.OnClick += HandleClick;
    }

    private void Start()
    {
        OnThresholdTimerTracked?.Invoke(earlyAmount, inTimeAmount, lateAmount);
    }

    private void OnDestroy()
    {
        ThresholdTimer.OnClick -= HandleClick;
    }


    private void HandleClick(ThresholdTimer threshold)
    {
        if (threshold.Early)
            earlyAmount++;
        else if (threshold.InTime)
            inTimeAmount++;
        else
            lateAmount++;
        OnThresholdTimerTracked?.Invoke(earlyAmount, inTimeAmount, lateAmount);
    }
}
